<?php

/**
 * Handler for extendRemoteControl Plugin for LimeSurvey : add yours functions here
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2017-2024 Denis Chenu <http://sondages.pro>
 * @license AGPL v3
 * @version 2.1.5
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

class RemoteControlHandler extends remotecontrol_handle
{
    /**
    * Add a response to the survey responses collection.
    * Returns the id of the inserted survey response
    *
    * @param string $sSessionKey Auth credentials
    * @param int $iSurveyID ID of the Survey to insert responses
    * @param struct $aResponseData The actual response
    * @param string $existingId if is is set : replace|replaceanswers|renumber|ignore
    * @param boolean $emCode allowed to use EM code
    * @param boolean $vvReplacement do the extra VV replacement ("{quote}", "{tab}", "{cr}", "{newline}", "{lbrace}")
    * @return int|array The response ID if success
    */
    public function add_response_code($sSessionKey, $iSurveyID, $aResponseData, $existingId = 'ignore', $emCode = true, $vvReplacement = true)
    {
        if (!$this->_checkSessionKey($sSessionKey)) {
            return array('status' => 'Invalid session key');
        }
        $oSurvey = Survey::model()->findByPk($iSurveyID);
        if (is_null($oSurvey)) {
            return array('status' => 'Error: Invalid survey ID');
        }
        if (!Yii::app()->db->schema->getTable('{{survey_' . $iSurveyID . '}}')) {
            return array('status' => 'No survey response table');
        }
        if (!Permission::model()->hasSurveyPermission($iSurveyID, 'responses', 'create') && !Permission::model()->hasSurveyPermission($iSurveyID, 'responses', 'import')) {
            return array('status' => 'No permission');
        }
        if (in_array($existingId, array('replace','replaceanswers')) && !Permission::model()->hasSurveyPermission($iSurveyID, 'responses', 'update')) {
            return array('status' => 'No update permission');
        }
        /* OK, we can import */
        $aRealFieldNames = Yii::app()->db->getSchema()->getTable(SurveyDynamic::model($iSurveyID)->tableName())->getColumnNames();
        LimeExpressionManager::SetDirtyFlag($iSurveyID); // Be sure survey EM code are up to date : think session is clean by default but :/
        $aLemFieldNames = LimeExpressionManager::getLEMqcode2sgqa($iSurveyID);

        $aResponse = array();
        foreach ($aResponseData as $code => $value) {
            if (in_array($code, $aRealFieldNames)) {
                $aResponse[$code] = $value;
            } elseif ($emCode && isset($aLemFieldNames[$code])) {
                $aResponse[$aLemFieldNames[$code]] = $value;
            }
        }
        if (empty($aResponse)) {
            return array('status' => 'No answers could be mapped.');
        }
        /* Null value for submitdate */
        if(isset($aResponseData['submitdate']) && empty($aResponseData['submitdate']) ) {
            unset($aResponseData['submitdate']);
        }
        /* datestamp & startdate diallow null. avoid DB issue */
        if ($oSurvey->datestamp == 'Y') {
            if (empty($aResponse['datestamp'])) {
                $aResponse['datestamp'] = date("Y-m-d H:i:s");
            }
            if (empty($aResponse['startdate'])) {
                $aResponse['startdate'] = date("Y-m-d H:i:s");
            }
        }
        /* startlanguage disallow null. avoid DB issue */
        if (empty($aResponse['startlanguage'])) {
            $aResponse['startlanguage'] = $oSurvey->language;
        }
        if (isset($aResponse['id']) && (string)$aResponse['id'] !== (string)(int)$aResponse['id']) {
            unset($aResponse['id']);
        }
        if (isset($aResponse['id'])) {
            $oResponse = Response::model($iSurveyID)->find('id=:id', array(":id" => $aResponse['id']));
            if ($oResponse) {
                switch ($existingId) {
                    case 'replace':
                        $oResponse->delete(true);
                        unset($oResponse);
                        break;
                    case 'replaceanswers':
                        unset($aResponse['id']);
                        break;
                    case 'renumber':
                        unset($aResponse['id']);
                        unset($oResponse);
                        break;
                    case 'ignore':
                    default:
                        return array('status' => 'Ignore exsiting id');
                }
            }
        }
        if (empty($oResponse)) {
            $oResponse = Response::create($iSurveyID);
        }
        foreach ($aResponse as $attribute => $value) {
            if ($value == '{question_not_shown}') {
                $oResponse->$attribute = new CDbExpression('NULL');
            } else {
                $valueFixed = $value;
                if ($vvReplacement) {
                    $valueFixed = str_replace(array("{quote}", "{tab}", "{cr}", "{newline}", "{lbrace}"), array("\"", "\t", "\r", "\n", "{"), $value);
                }
                $oResponse->setAttribute($attribute, $valueFixed);
            }
        }
        $oTransaction = Yii::app()->db->beginTransaction();
        try {
            if (isset($aResponse['id'])) {
                switchMSSQLIdentityInsert('survey_' . $iSurveyID, true);
            }
            if ($oResponse->save()) {
                if (isset($aResponse['id'])) {
                    switchMSSQLIdentityInsert('survey_' . $iSurveyID, false);
                }
                $oTransaction->commit();
                return $oResponse->id;
            }
            $oTransaction->rollBack();
            if (isset($aResponse['id'])) {
                switchMSSQLIdentityInsert('survey_' . $iSurveyID, false);
            }
            $errors = $oResponse->getErrors();
            return array('status' => 'Unable to add response : {$errors[0]}' ); // Return 1st error
        } catch (Exception $oException) {
            return array('status' => 'Unable to add response : ' . $oException->getMessage());
        }
        /*Can not come here */
        return array('status' => 'Unable to add response'); // Add validation error ?
    }

    /**
    * RPC Routine to get global permission of the actual user
    *
    * @access public
    * @param string $sSessionKey Auth credentials
    * @param string $sPermission string Name of the permission - see function getGlobalPermissions
    * @param $sCRUD string The permission detailsyou want to check on: 'create','read','update','delete','import' or 'export'
    * @return bool True if user has the permission
    * @return boolean
    */
    public function hasGlobalPermission($sSessionKey, $sPermission, $sCRUD = 'read')
    {
        $this->_checkSessionKey($sSessionKey);
        return array(
            'permission' => \Permission::model()->hasGlobalPermission($sPermission, $sCRUD)
        );
    }

    /**
    * RPC Routine to get survey permission of the actual user
    *
    * @access public
    * @param string $sSessionKey Auth credentials
    * @param $iSurveyID integer The survey ID
    * @param $sPermission string Name of the permission
    * @param $sCRUD string The permission detail you want to check on: 'create','read','update','delete','import' or 'export'
    * @return bool True if user has the permission
    * @return boolean
    */
    public function hasSurveyPermission($sSessionKey, $iSurveyID, $sPermission, $sCRUD = 'read')
    {
        $this->_checkSessionKey($sSessionKey);
        return array(
            'permission' => \Permission::model()->hasSurveyPermission($iSurveyID, $sPermission, $sCRUD),
        );
    }
}
